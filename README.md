# Joels OCR-skript

A small script that uses Tesseract and Imagemagick to ocr image files and pdfs. Returns the result as a txt-file in the working directory.


## Usage

```bash
./ocr swe /path/to/file.png
```

or

```bash
./ocr swe /path/to/directory pdf
```
